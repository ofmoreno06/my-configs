" ==============================================================
" vim config file
" ==============================================================

" =========================================
" vundle
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" ================================
" Add your plugins after this line
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdtree'
Plugin 'tpope/vim-surround'
Plugin 'valloric/youcompleteme'
Plugin 'pangloss/vim-javascript'
Plugin 'mattn/emmet-vim'
Plugin 'leafgarland/typescript-vim'

" Add your plugins before this line
" ================================

call vundle#end()            " required
filetype plugin indent on    " required
" =========================================

" =========================================
" PLUGINS CONFIG AND MAPPING
map <C-n> :NERDTreeToggle<CR>

" =========================================
" CUSTOM OPTIONS 

" tab/indent
syntax enable
set tabstop=2
set shiftwidth=2 expandtab
set autoindent
set smartindent
set pastetoggle=<f5>

" numbers
set relativenumber
